#include <stdio.h>

int main(void) {
    
    int A, x = 1;
    
    scanf("%d", &A);
    
    while (x <= A) {
        int t = (A / x) % 10;
        x *= 10;
        if (t >= 5) {
            A = (A / x + 1) * x;
        }
    }
    
    printf("%d\n", A);
    
    return 0;
}
