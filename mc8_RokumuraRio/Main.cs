// Try Paiza Moshijo
// author: Leonardone @ NEETSDKASU
using System;

public class Hello
{
    public static void Main()
    {
        var lines = Console.ReadLine().Split(' ');
        
        Console.WriteLine((new Uri(new Uri("file://" + lines[0] + "/"), lines[1])).LocalPath);
    }
}
