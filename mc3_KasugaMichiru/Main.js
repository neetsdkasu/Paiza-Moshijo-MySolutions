// Try Paiza Moshijo
// author: Lenardone @ NEETSDKASU
process.stdin.resume();
process.stdin.setEncoding('utf8');
(function() {
    var lines = '';
    process.stdin.on('data', function (chunk) {
        lines += chunk.toString();
    });
    process.stdin.on('end', function () {
        if (lines.match(/help/)) {
            console.log('SOS');
        } else {
            console.log(lines.trim());
        }
    });
})();