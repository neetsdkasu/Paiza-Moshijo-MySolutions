# Try Paiza Moshijo
# author: Leonardone @ NEETSDKASU
############################################################
def gs() gets.strip end
def gi() gets.to_i end
def gss() gs.split end
def gis() gss.map(&:to_i) end
def nmapf(n,f) n.times.map{ __send__ f } end
def ngs(n) nmapf n,:gs end
def ngi(n) nmapf n,:gi end
def ngss(n) nmapf n,:gss end
def ngis(n) nmapf n,:gis end
def arr2d(h,w,v=0) h.times.map{[v] * w} end
def for2p(hr,wr,&pr) hr.each{|i|wr.each{|j| yield(i,j)}} end
def nsum(n) n * (n + 1) / 2 end
def vcount(d,r=Hash.new(0)) d.inject(r){|m,e| m[e]+=1;m} end
############################################################

H, W, N = gis
g = ngs H
sx, sy = gis.map &:pred
tx, ty = gis.map &:pred

c = arr2d H, W, nil

c[sy][sx] = [0, '']

S = [[-1, 0, 'U'], [0, 1, 'R'], [1, 0, 'D'], [0, -1, 'L']]
l1 = [[sy, sx]]

while !l1.empty?
    l2 = []
    l1.each do |y, x|
        d, r = c[y][x]
        S.each do |dy, dx, s|
            nx, ny = x + dx, y + dy
            next if !(0 ... H).include? ny
            next if !(0 ... W).include? nx
            next if g[ny][nx] == '#'
            ad = d + (g[ny][nx] == '.' ? 1 : 100)
            if c[ny][nx].nil? || ad < c[ny][nx][0]
                c[ny][nx] = [ad, r + s]
                l2 << [ny, nx]
            end
        end
    end
    l1 = l2.uniq
end

puts c[ty][tx][1].each_char.to_a * "\n"

