// Try paiza Moshijo
// author: Leonardone @ NEETSDKASU
package main

import (
	"bufio"
	"container/heap"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Point struct{ X, Y int }

func add(p1, p2 Point) Point { return Point{p1.X + p2.X, p1.Y + p2.Y} }

type FieldSize struct{ Width, Height int }

func (s *FieldSize) in(p Point) bool { return 0 <= p.X && p.X < s.Width && 0 <= p.Y && p.Y < s.Height }

type FieldData [200]string

func (f *FieldData) get(p Point) byte { return f[p.Y][p.X] }

type ProblemData struct {
	Size                 FieldSize
	TrapCount            int
	Field                FieldData
	StartPoint, EndPoint Point
	KeyPoints            [26]Point
}

func ReadProblemData(r io.Reader) *ProblemData {
	var scn = bufio.NewScanner(r)

	var readLine = func() string { scn.Scan(); return scn.Text() }
	var scanInts = func(ys ...*int) {
		for i, x := range strings.Split(readLine(), " ") {
			*ys[i], _ = strconv.Atoi(x)
		}
	}

	data := new(ProblemData)

	scanInts(&data.Size.Height, &data.Size.Width, &data.TrapCount)

	for i := 0; i < data.Size.Height; i++ {
		data.Field[i] = readLine()
	}

	var p Point

	scanInts(&p.X, &p.Y)
	data.StartPoint = Point{X: p.X - 1, Y: p.Y - 1}

	scanInts(&p.X, &p.Y)
	data.EndPoint = Point{X: p.X - 1, Y: p.Y - 1}

	for y := 0; y < data.Size.Height; y++ {
		for x := 0; x < data.Size.Width; x++ {
			c := data.Field[y][x]
			if c == '.' || c == '#' || c < 'a' || 'z' < c {
				continue
			}
			data.KeyPoints[int(c)-'a'] = Point{X: x, Y: y}
		}
	}

	return data
}

type MoveFrom int

const (
	N MoveFrom = iota
	L
	U
	R
	D
)

type Flag int

func (f *Flag) set(index int)     { *f |= 1 << uint(index) }
func (f *Flag) erase(index int)   { *f &^= 1 << uint(index) }
func (f Flag) get(index int) bool { return f&(1<<uint(index)) != 0 }
func (f Flag) count() (c int) {
	for f > 0 {
		if f&1 == 1 {
			c++
		}
		f >>= 1
	}
	return
}

type Node struct {
	Distance, TrapCount int
	From                MoveFrom
	TrapFlag            Flag
}

const TrapDamage = 100

func (n *Node) cost() int { return n.Distance + n.TrapCount*TrapDamage }
func (n *Node) setTrap(ch byte) {
	n.TrapCount++
	n.TrapFlag.set(int(ch) - 'A')
}

const GoalIndex = 26

var DistanceTable = new([27][200][200]*Node)

var moveToList = [4]Point{
	{1, 0}, {0, 1}, {-1, 0}, {0, -1},
}

func MakeDistanceTable(field *FieldData, size FieldSize, index int, start Point) {
	table := &DistanceTable[index]
	table[start.Y][start.X] = new(Node)
	var tmp1 []Point
	tmp1 = append(tmp1, start)
	for len(tmp1) > 0 {
		var tmp2 []Point
		for _, p := range tmp1 {
			n := table[p.Y][p.X]
			dmg := n.cost() + 1
			for m, d := range moveToList {
				tp := add(p, d)
				if !size.in(tp) {
					continue
				}
				ch := field.get(tp)
				if ch == '#' {
					continue
				}
				isTrap := 'A' <= ch && ch <= 'Z'
				if isTrap {
					dmg += TrapDamage
				}
				tn := table[tp.Y][tp.X]
				if tn != nil && dmg >= tn.cost() {
					//if tn != nil && n.Distance + 1 >= tn.Distance {
					continue
				}
				if tn == nil {
					table[tp.Y][tp.X] = new(Node)
					tn = table[tp.Y][tp.X]
				}
				*tn = *n
				tn.Distance++
				if isTrap {
					tn.setTrap(ch)
				}
				tn.From = MoveFrom(m + 1)
				tmp2 = append(tmp2, tp)
			}
		}
		tmp1, tmp2 = tmp2, tmp1
	}
}

type Root struct {
	index, damage int
	key           Flag
	back          *Root
}

type RootList []*Root

func (r RootList) Len() int            { return len(r) }
func (r RootList) Less(i, j int) bool  { return r[i].damage < r[j].damage }
func (r RootList) Swap(i, j int)       { r[i], r[j] = r[j], r[i] }
func (r *RootList) Push(x interface{}) { *r = append(*r, x.(*Root)) }
func (r *RootList) Pop() interface{} {
	old := *r
	n := len(old)
	item := old[n-1]
	*r = old[0 : n-1]
	return item
}

const (
	LoopCount = 500000
	HeapSize  = 30000
)

func Solve(data *ProblemData) (res string) {
	MakeDistanceTable(&data.Field, data.Size, GoalIndex, data.EndPoint)
	for i := 0; i < data.TrapCount; i++ {
		MakeDistanceTable(&data.Field, data.Size, i, data.KeyPoints[i])
	}

	ans := &Root{
		index:  GoalIndex,
		damage: DistanceTable[GoalIndex][data.StartPoint.Y][data.StartPoint.X].cost(),
		key:    0,
		back:   nil,
	}

	pq := make(RootList, 0, HeapSize+30)
	for i := 0; i < data.TrapCount; i++ {
		nd := DistanceTable[i][data.StartPoint.Y][data.StartPoint.X]
		if nd != nil {
			pq = append(pq, &Root{
				index:  i,
				damage: nd.cost(),
				key:    1 << uint(i),
				back:   nil,
			})
		}
	}
	heap.Init(&pq)

	for j := 0; j < LoopCount; j++ {
		if len(pq) == 0 {
			break
		}
		root := heap.Pop(&pq).(*Root)
		pos := &data.KeyPoints[root.index]
		if nd := DistanceTable[GoalIndex][pos.Y][pos.X]; nd != nil {
			flag := nd.TrapFlag
			flag &^= root.key
			total := root.damage + nd.Distance + flag.count()*TrapDamage
			if total < ans.damage {
				ans = &Root{
					index:  GoalIndex,
					damage: total,
					key:    root.key,
					back:   root,
				}
			}
		}
		for k := 0; k < data.TrapCount; k++ {
			if root.key.get(k) {
				continue
			}
			if nd := DistanceTable[k][pos.Y][pos.X]; nd != nil {
				flag := nd.TrapFlag
				flag &^= root.key
				damage := root.damage + nd.Distance + flag.count()*TrapDamage
				if damage >= ans.damage { continue }
				heap.Push(&pq, &Root{
					index:  k,
					damage: damage ,
					key:    root.key | (1 << uint(k)),
					back:   root,
				})
			}
		}
		if len(pq) > HeapSize {
			pq = pq[0:HeapSize]
		}
	}

	path := make(RootList, 0, 27)
	for r := ans; r != nil; r = r.back {
		path = append(path, r)
	}

	pos := data.StartPoint
	for i := len(path) - 1; i >= 0; i-- {
		index := path[i].index
		table := &DistanceTable[index]
		var reach Point
		if index < 26 {
			reach = data.KeyPoints[index]
		} else {
			reach = data.EndPoint
		}
		for pos != reach {
			switch table[pos.Y][pos.X].From {
			case L:
				res += "L"
				pos.X--
			case U:
				res += "U"
				pos.Y--
			case R:
				res += "R"
				pos.X++
			case D:
				res += "D"
				pos.Y++
			}
		}
	}

	return
}

func DoIt() {
	data := ReadProblemData(os.Stdin)
	ans := Solve(data)
	for _, c := range ans {
		fmt.Println(string(c))
	}
}

func main() {
	DoIt()
}
