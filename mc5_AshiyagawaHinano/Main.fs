// Try Paiza Moshijo
// author: Leonardone @ NEETSDKASU


[<EntryPoint>]
let main _ =
    let table = stdin.ReadLine().Split(' ') |> Array.map (fun s -> s.[0])
    match stdin.ReadLine() with
    | "encode" ->
        stdin.ReadLine()
        |> String.iter (fun c ->
            printf "%c" table.[int c - int '0']
            )
    | "decode" ->
        stdin.ReadLine()
        |> String.iter (fun c ->
            Array.findIndex ((=) c) table
            |> printf "%d"
            )
    | cmd ->
        failwith cmd
    0