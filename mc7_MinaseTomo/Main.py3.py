# coding: utf-8
# Try Paiza Moshijo
# author: Leonardone @ NEETSDKASU

n = int(input())
psd = [list(map(int, input().split())) for _ in range(n)]
m = int(input())
for _ in range(m):
    c, a = map(int, input().split())
    p = psd[c - 1][0] * a - psd[c - 1][2] * (a // psd[c - 1][1])
    print(p)
